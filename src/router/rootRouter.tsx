import React, { Component } from 'react'
import { Route } from 'react-router'
import { connect } from 'react-redux'
// import custom components
import AuthPage from '../pages/Auth/index'
import Registartion from '../pages/Registration'
import Profile from '../pages/Profile'
import PasswordRecovery from '../pages/PasswordRecovery'
import { PrivateRoute } from '../components/PrivateRoute'
import EditedForm from '../pages/Profile/components/EditedForm'

class RootRouter extends Component<any> {
	render() {
		return (
			<>
				<Route path="/" component={AuthPage} exact />
				<Route path="/signin" component={Registartion} />
				<PrivateRoute
					isLogin={
						(this.props.auth != undefined && this.props.auth.data.result) || this.props.token
							? true
							: false
					}
					component={Profile}
					path="/profile"
					exact
				/>
				<PrivateRoute
					isLogin={
						(this.props.auth != undefined && this.props.auth.data.result) || this.props.token
							? true
							: false
					}
					component={EditedForm}
					path="/profile/:form"
				/>
				<Route path="/password_recovery" component={PasswordRecovery} />
			</>
		)
	}
}

const mapStateToProps = (state: any) => {
	return {
		auth: state.auth,
		token: state.token,
	}
}

export default connect(mapStateToProps)(RootRouter)
