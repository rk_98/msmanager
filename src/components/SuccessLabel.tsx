import styled from 'styled-components';

export const SuccessLabel = styled.p`
	width: 190px;
	font-size: 0.8em;
	color: #009800;
	text-align: center
`;