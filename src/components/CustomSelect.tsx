import React from 'react'
import styled from 'styled-components'
import { Select, InputLabel, MenuItem, FormControl } from '@material-ui/core'

export const CustomSelect = ({
	field,
	form: { touched, errors },
	options,
	label,
	...props
}: any & { options: any, label: string }) => (
	<StyledSelect>
		<InputLabel htmlFor={field.name}>{label}</InputLabel>
		<Select
			onChange={field.onChange}
			value={field.value}
			defaultValue={''}
			inputProps={{
				name: field.name,
				id: field.name,
			}}
			{...props}
		>
			{ options != undefined && options.map((item: any, index: number) => (
				<MenuItem key={index} value={item.variant}>
					{item.description}
				</MenuItem>
			))}
		</Select>
	</StyledSelect>
)

const StyledSelect = styled<any>(FormControl)`
  width: 100%
  margin: 5px 0px !important;
`
