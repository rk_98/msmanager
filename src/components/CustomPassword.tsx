import {ErrorMessage, FieldProps} from "formik";
import React from "react";
import styled from "styled-components";
import TextField from "@material-ui/core/TextField";

export const CustomPassword = ({field, form: {touched, errors}, ...props}: FieldProps) => (
  <CustomFormElem>
    <ErrorMessage component="span" name={field.name}/>
    <CustomTextField margin="normal" type="password" {...field} {...props} />
  </CustomFormElem>
);

const CustomFormElem = styled.div`
  margin: 5px 0px;
  span{
    display: block;
    color: red;
  }
  select{
    margin: 5px 0px;
    padding: 7px 0px;
  }
`;

const CustomTextField = styled<any>(TextField)`
  width: 100%;
  margin: 5px 0px;
`;