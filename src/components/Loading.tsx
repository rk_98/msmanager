import styled from 'styled-components';

export const Loading = styled.div`
	text-transform: uppercase;
	margin-top: 10px;
	font-size: 0.8em
`;