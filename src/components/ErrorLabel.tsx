import styled from 'styled-components';

export const ErrorLabel = styled.p`
	width: 190px;
	font-size: 0.8em;
	color: #da0606;
`;