import { ErrorMessage, FieldProps } from 'formik'
import React from 'react'
import styled from 'styled-components'
import MaskedInput from 'react-text-mask'

export const CustomMasked = ({ field, form: { touched, errors }, ...props }: FieldProps & {placeholder: string, mask:string}) => (
	<CustomFormElem>
		<ErrorMessage component="span" name={field.name} />
		<CustomTextField margin="normal" mask={props.mask} label={props.placeholder} type="text" {...field} {...props} />
	</CustomFormElem>
)

const CustomFormElem = styled.div`
	margin: 5px 0px;
	span {
		display: block;
		color: red;
	}
	select {
		margin: 5px 0px;
		padding: 7px 0px;
	}
`

const CustomTextField = styled<any>(MaskedInput)`
	width: calc(100% - 3px);
	margin: 5px 0px;
	border-radius: 0px;
	border: 0px;
	border-bottom-color: currentcolor;
	border-bottom-style: none;
	border-bottom-width: 0px;
	padding: 7px 0px;
	border-bottom: 1px solid #949494;
`
