import React, { Component } from 'react'
import { AppBar, Typography, Button, Toolbar, IconButton } from '@material-ui/core'
import { KeyboardArrowLeft } from '@material-ui/icons'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

interface IProps {
	title: string;
	backButton?: boolean;
}

class AppBarComponent extends Component<IProps> {
	render() {
		return (
			<AppBar position="static" color="primary">
				<StyledToolbar>
					{this.props.backButton && (
						<Link to="/profile">
							<IconButton aria-label="KeyboardArrowLeft">
								<KeyboardArrowLeft fontSize="small"/>
							</IconButton>
						</Link>
					)}
					<Typography variant="h6" color="inherit">
						{this.props.title}
					</Typography>
					<Button variant="contained" color="primary">
						Выйти
					</Button>
				</StyledToolbar>
			</AppBar>
		)
	}
}

const StyledToolbar = styled(Toolbar)`
	justify-content: space-between;
`

export default AppBarComponent
