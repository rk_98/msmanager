import styled from 'styled-components';

export const MainWrap = styled.div`
background-color: #f2f2f2;
min-height: calc(100vh - 40px);
display: flex;
align-items: center;
padding: 20px;
justify-content: center;
flex-direction: column;
`;