export const LOGOUT: string = 'LOGOUT'

export const logout = () => {
	return {
		type: LOGOUT,
	}
}

export const GET_MAIN_STARTED: string = 'GET_MAIN_STARTED'
export const GET_MAIN_REQUEST: string = 'GET_MAIN_REQUEST'
export const GET_MAIN_SUCCESS: string = 'GET_MAIN_SUCCESS'
export const GET_MAIN_FAILURE: string = 'GET_MAIN_FAILURE'

export const getMainStarted = (token: string) => {
	return {
		type: GET_MAIN_STARTED,
		payload: token,
	}
}

export const getMainRequest = () => {
	return {
		type: GET_MAIN_REQUEST,
	}
}

export const getMainSuccess = (data: object) => {
	return {
		type: GET_MAIN_SUCCESS,
		payload: data,
	}
}

export const getMainFailure = (error: object) => {
	return {
		type: GET_MAIN_FAILURE,
		payload: error,
	}
}

export const GET_SINGLE_PROFILE: string = 'GET_SINGLE_PROFILE'
export const GET_SINGLE_PROFILE_REQUEST: string = 'GET_SINGLE_PROFILE_REQUEST'
export const GET_SINGLE_PROFILE_SUCCESS: string = 'GET_SINGLE_PROFILE_SUCCESS'
export const GET_SINGLE_PROFILE_FAILURE: string = 'GET_SINGLE_PROFILE_FAILURE'

export const getSingleProfile = (form:object) => {
	return {
		type: GET_SINGLE_PROFILE,
		payload: form,
	}
}

export const getSingleReqest = () => {
	return {
		type: GET_SINGLE_PROFILE_REQUEST,
	}
}

export const getSingleSuccess = (data: object) => {
	return {
		type: GET_SINGLE_PROFILE_SUCCESS,
		payload: data,
	}
}

export const getSingleFailure = (error: object) => {
	return {
		type: GET_SINGLE_PROFILE_FAILURE,
		payload: error
	}
}