import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Formik, Field, FieldArray, Form } from 'formik'
import axios from 'axios'

import AppBarComponent from '../../../../components/AppBar'
import { MainWrap } from '../../../../components/MainWrapper'
import { getSingleProfile } from '../../actions/actions'
import { Typography, CircularProgress, Grid, Button, Table, TableBody, TableHead, TableCell, TableRow } from '@material-ui/core'
import { CustomInput } from '../../../../components/CustomInput'
import { CustomSelect } from '../../../../components/CustomSelect'
import { CustomMasked } from '../../../../components/CustomMasked'
import { string } from 'prop-types'

interface IProps {
	match: any;
	token: string;
	getSingleProfile: (values: object) => void;
	formData: any;
	auth: any;
	variants: object;
}

class EditedForm extends Component<IProps, any> {

	state:any = {}

	getVariants(variants: any, owner: string) {
		const select = variants[owner]
		return select
	}

	getCatalogVariants(item: string, catalog: string) {
		axios({
			url: 'http://msmanager.ru:45080/53c6ffac-338e-11e9-a794-002590e9c4b0_service/hs/agent-api/v2',
			headers: { 'content-type': 'text/plain' },
			method: 'post',
			data: {
				method: 'GetCatalog',
				params: {
					JWT: this.props.token || this.props.auth.data.result,
					catalog: catalog,
					parent: '',
				},
			},
		}).then(response => {
			this.setState((prevState: any) => {
				prevState[catalog] = response.data.result;
				return prevState;
			});
		})
	}

	componentDidMount() {
		const paramsForSaga = {
			token: this.props.token == '' ? this.props.auth.data.result : this.props.token,
			form: this.props.match.params.form,
		}
		this.props.getSingleProfile(paramsForSaga)
	}

	componentWillReceiveProps(nextProps: any) {
		if (nextProps.formData != undefined) {
			const containerCatalog = nextProps.formData.data.result.sections
			containerCatalog.map((section: any, index: number) => {
				section.content.map((input: any) => {
					if (input.question.type == 'catalog') {
						const data = this.getCatalogVariants(input.item, input.question.catalog);
					}
				})
			})
		}
	}

	getCatalogOptionsWithVariant(stateElement: []) {
		if (stateElement) return stateElement.map((el: {item: string}) => ({...el, variant: el.item}))
	}

	componentWillUnmount(){
		this.setState((prevState: any) => {
			prevState = undefined;
			return prevState;
		}) 
		console.log(this.state)
	}

	render() {
		const form: any = this.props.formData != undefined ? this.props.formData.data.result : {}
		return (
			<MainWrapProfile>
				<AppBarComponent title="Редактирование формы" backButton={true} />
				{this.props.formData != undefined ? (
					<ContentWrap>
						<Grid container spacing={16}>
							<Grid item xs={12}>
								<Formik
									initialValues={form.answers}
									onSubmit={(values: object) => {
										console.log(values)
									}}
									render={() => (
										<Form>
											<Typography variant="h6">{form.caption}</Typography>
											<Field hidden disabled name="version" defaultValues={form.version} />

											{form != undefined &&
												form.sections.map((section: any, index: number) => (
													<SectionWrapper key={index}>
														<p>{section.section}</p>
														{section.content.map((input: any) => {
															switch (input.question.type) {
																case 'string':
																	if(input.question.mask !== ""){
																		return (
																			<Field 
																				component={CustomMasked}
																				name={input.item}
																				placeholder={input.question.description}
																				key={input.item}
																				defaultValue={form.answers[input.item]}
																				mask={[ '+', /[0-9]/,'(',/[0-9]/,/[0-9]/,/[0-9]/,')',/[0-9]/,/[0-9]/,/[0-9]/,'-',/[0-9]/,/[0-9]/,'-',/[0-9]/,/[0-9]/]}
																			/>
																		)
																	} else{
																		return (
																			<Field
																				component={CustomInput}
																				name={input.item}
																				placeholder={input.question.description}
																				key={input.item}
																				defaultValue={form.answers[input.item]}
																			/>
																		)
																	}

																case 'text':
																	return <p key={input.item}>это textarea</p>
																case 'time':
																	return <p key={input.item}>это поле со временем</p>

																case 'one':
																	return (
																		<Field
																			component={CustomSelect}
																			name={input.item}
																			label={input.question.description}
																			key={input.item}
																			defaultValue={form.answers[input.item]}
																			options={this.getVariants(
																				form.variants,
																				input.question.owner
																			)}
																		/>
																	)

																case 'some':
																	return (
																		<Field
																			//multiple
																			component={CustomSelect}
																			name={input.item}
																			label={input.question.description}
																			key={input.item}
																			defaultValue={form.answers[input.item]}
																			options={this.getVariants(
																				form.variants,
																				input.question.owner
																			)}
																		/>
																	)

																case 'table':
																		return (
																			<StyledTable>
																			<TableHead>
																				<TableRow>
																					<TableCell colSpan={2}>{input.question.description}</TableCell>
																				</TableRow>
																			</TableHead>
																			<TableBody>
																				<TableRow>
																				{form.table_questions[input.question.question].map(
																					(item: any, index: number) => (
																						<TableCell key={index}>test</TableCell>
																					)
																				)}
																				</TableRow>
																			</TableBody>
																		</StyledTable>
																		)
																		
																case 'catalog':
																	return (
																		<Field
																			component={CustomSelect}
																			name={input.item}
																			label={input.question.description}
																			key={input.item}
																			//defaultValue={form.answers[input.item]}
																			options={
																				this.getCatalogOptionsWithVariant(this.state[input.question.catalog])
																			}
																		/>
																	)
																case 'number':
																	return <p>поле с числом</p>
																case 'date':
																	return (
																			<Field 
																				component={CustomMasked}
																				name={input.item}
																				placeholder={ `${input.question.description} (число/месяц/год)`}
																				key={input.item}
																				defaultValue={form.answers[input.item]}
																				mask={[/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/]}
																				
																			/>
																		)
																default:
																	return (
																		<Field
																			component={CustomSelect}
																			name={input.item}
																			label={input.question.description}
																			key={input.item}
																			defaultValue={form.answers[input.item]}
																			options={ [{variant: true, description: 'Да'},{variant: false, description: 'Нет'}] }
																		/>
																	)
															}
														})}
													</SectionWrapper>
												))}
											<Button type="submit" color="primary" variant="contained">Отправить</Button>
										</Form>
									)}
								/>
							</Grid>
						</Grid>
					</ContentWrap>
				) : (
					<CircularProgress />
				)}
			</MainWrapProfile>
		)
	}
}

const StyledTable = styled(Table)`
	margin: 5px 0px 10px;
`

const SectionWrapper = styled.div`
	background-color: #fff;
	padding: 1px 20px 10px;
	border-radius: 4px;
	margin: 5px 0px;
`

const MainWrapProfile = styled(MainWrap)`
	justify-content: flex-start;
	padding: 0px;
	min-height: 100vh;
`

const ContentWrap = styled.div`
	padding: 20px;
	width: calc(100vw - 60px);
`

const mapStateToProps = (state: any) => {
	return {
		token: state.token,
		loading: state.loading,
		formData: state.formData,
		auth: state.auth,
	}
}

const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		getSingleProfile: bindActionCreators(getSingleProfile, dispatch),
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditedForm)
