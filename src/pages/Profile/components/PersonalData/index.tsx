import React, { Component } from 'react'
import styled from 'styled-components'
import { Button } from '@material-ui/core'
import { Link } from 'react-router-dom'

import { SuccessLabel } from '../../../../components/SuccessLabel'
import { ErrorLabel } from '../../../../components/ErrorLabel'

interface IProps {
	data: any;
}

class Personal extends Component<IProps> {
	render() {
		return (
			<PersonalWrap>
				{this.props.data != undefined && this.props.data.relevance && (
					<StyledSucces>
						<LabelSpan>{this.props.data.description}</LabelSpan>
					</StyledSucces>
				)}
				{this.props.data != undefined && !this.props.data.relevance && (
					<StyledError>
						<LabelSpan>{this.props.data.description}</LabelSpan>
						<Link to={`/profile/${this.props.data.form}`}>
							<Button variant="outlined" color="primary">
								Заполнить
							</Button>
						</Link>
					</StyledError>
				)}
			</PersonalWrap>
		)
	}
}

const PersonalWrap = styled.div``

const StyledSucces = styled(SuccessLabel)`
	width: auto;
	font-size: 0.9em;
	margin: 5px 0px 0px;
`

const StyledError = styled(ErrorLabel)`
	width: auto;
	font-size: 0.9em;
	margin: 5px 0px 0px;
	align-items: start;
	display: flex;
	flex-direction: column;
`

const LabelSpan = styled.span`
	margin-bottom: 15px;
`

export default Personal
