import axios from 'axios';
import {call, put} from 'redux-saga/effects';
import {getSingleReqest, getSingleSuccess, getSingleFailure} from '../actions/actions';

export function* getSingle(action: any) {
    try{
        yield put(getSingleReqest());
        const data = yield call(()=>{
            return axios({
                url: 'http://msmanager.ru:45080/53c6ffac-338e-11e9-a794-002590e9c4b0_service/hs/agent-api/v2',
				headers: {'content-type':'text/plain'},
				method: "post",
				data: {
					method: "GetForm",
					params: {
                        JWT: action.payload.token,
                        form: action.payload.form
					}
				}
            })
        });
        yield put(getSingleSuccess(data));
    } catch(error){
        yield put(getSingleFailure(error.message));
    }
}