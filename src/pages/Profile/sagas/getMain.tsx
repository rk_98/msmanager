import axios from 'axios';
import {call, put} from 'redux-saga/effects';
import {getMainRequest, getMainSuccess, getMainFailure} from '../actions/actions'

export function* getMainData(action: any) {
	try{
		yield put(getMainRequest());
		const data = yield call(()=> {
			return axios({
				url: 'http://msmanager.ru:45080/53c6ffac-338e-11e9-a794-002590e9c4b0_service/hs/agent-api/v2',
				headers: {'content-type':'text/plain'},
				method: "post",
				data: {
					method: "GetMain",
					params: {
						JWT: action.payload
					}
				}
			})
		});
		if(data.data.error){
			throw new Error('token not found')
		}
		yield put(getMainSuccess(data));
	} catch(error){
		yield put(getMainFailure(error.message));
	}
}