import React, { Component } from 'react'
import { Card, CardContent, Grid, CircularProgress } from '@material-ui/core'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { Dispatch, bindActionCreators } from 'redux'
import { logout, getMainStarted } from './actions/actions'

import { MainWrap } from '../../components/MainWrapper'
import Personal from './components/PersonalData/'
import AppBarComponent from '../../components/AppBar'
import {ErrorLabel} from '../../components/ErrorLabel'

interface IProps {
	loading: boolean
	auth: any
	logout: () => void
	getMainStarted: any
	token: string
	profile: any
}

class Profile extends Component<IProps> {
	componentDidMount() {
		if (this.props.auth != undefined) {
			this.props.getMainStarted(this.props.auth.data.result)
		} else {
			this.props.getMainStarted(this.props.token)
		}
	}

	signOut() {
		this.props.logout()
	}

	render() {
		return (
			<MainWrapProfile>
				<AppBarComponent title="Профиль" />
				<ContentWrap>
					<Grid container spacing={16}>
						<Grid xs={12} item>
						<SectionTitle>Персональная анкета</SectionTitle>
							<Card>
								<CardContent>
									{this.props.loading ? (
										<CircularProgress color="primary" />
									) : (
										<Personal
											data={
												this.props.profile != undefined
													? this.props.profile.data.result.agent
													: undefined
											}
										/>
									)}
								</CardContent>
							</Card>
						</Grid>
						<Grid xs={12} item>
							<SectionTitle>Анкеты объектов</SectionTitle>
						</Grid>
							{this.props.loading ? (
								<CircularProgress color="primary" />
							) : this.props.profile != undefined &&
							  this.props.profile.data.result.checks.length == 0 ? (
								<ErrorLabel>анкет для заполнения не найдено</ErrorLabel>
							) : this.props.profile != undefined && this.props.profile.data.result.checks.map( (item:any, index: number) => (

								<Grid xs={12} sm={6} item key={index}>
									<Card>
										<CardContent>
											<ProfileInformation>Объект: <strong>{item.object}</strong></ProfileInformation>
											<ProfileInformation>Вы <strong>{item.role}</strong></ProfileInformation>
											<Personal data={item}/>
										</CardContent>
									</Card>
								</Grid>
							))}
					</Grid>
				</ContentWrap>
			</MainWrapProfile>
		)
	}
}

const ProfileInformation = styled.p`
	font-size: 0.9em;
	margin: 0px;
`

const SectionTitle = styled.p`
	font-size: 1.2em
`

const MainWrapProfile = styled(MainWrap)`
	justify-content: flex-start;
	padding: 0px;
	min-height: 100vh;
`

const ContentWrap = styled.div`
	padding: 20px;
	width: calc(100vw - 60px);
`

const mapStateToProps = (state: any) => {
	return {
		loading: state.loading,
		auth: state.auth,
		token: state.token,
		profile: state.mainData,
	}
}

const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		logout: bindActionCreators(logout, dispatch),
		getMainStarted: bindActionCreators(getMainStarted, dispatch),
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Profile)
