import {call, put} from 'redux-saga/effects';
import axios from 'axios';
import {requestRecovery, successRecovery, failureRecovery} from '../actions/actions';

export function* recoveryPassword(action: any){
	try{
		yield put(requestRecovery());
		console.log(action);
		const data = yield call( () => {
			return axios({
				url: 'http://msmanager.ru:45080/53c6ffac-338e-11e9-a794-002590e9c4b0_service/hs/agent-api/v2',
				method: "post",
				headers: {'content-type':'text/plain'},
				data: {
					method: "UserRestore",
					params: {
						email: action.payload.email
					}
				}
			});
		});
		yield put(successRecovery(data));
	} catch(error){
		yield put(failureRecovery(error));
	}
}