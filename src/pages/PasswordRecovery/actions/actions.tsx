export const STARTED_RECOVERY:string = "STARTED_RECOVERY";
export const REQUEST_RECOVERY:string = "REQUEST_RECOVERY";
export const SUCCESS_RECOVERY:string = "SUCCESS_RECOVERY";
export const FAILURE_RECOVERY:string = "FAILURE_RECOVERY";

export const startedRecovery = (email: string) => {
    return{
        type: STARTED_RECOVERY,
        payload: email
    }
}

export const requestRecovery = () => {
    return{
        type: REQUEST_RECOVERY
    }
}

export const successRecovery = (data: object) => {
    return{
        type: SUCCESS_RECOVERY,
        payload: data
    }
}

export const failureRecovery = (error: object) => {
    return{
        type: FAILURE_RECOVERY,
        payload: error
    }
}