import React, {Component} from 'react';
import {Card, CardContent, Button} from '@material-ui/core';
import {Formik, Form, Field} from 'formik';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import {MainWrap} from '../../components/MainWrapper';
import {CustomInput} from '../../components/CustomInput';
import {validate} from '../../components/validate';
import {Loading} from '../../components/Loading';
import {SuccessLabel} from '../../components/SuccessLabel';
import {ErrorLabel} from '../../components/ErrorLabel';
import {startedRecovery} from './actions/actions';

const initState = {
	email: ''
}

interface IProps {
	loading: boolean,
	data: any,
	error: object,
	startedRecovery: (value:any) => void
}

class PasswordRecovery extends Component<IProps>{
	render(){
		return(
			<MainWrap>
				<Card>
					<CardContent>
						{!this.props.loading ?
						<>
						{this.props.data == undefined || !this.props.data.data.result ?
						<>
						<HeadingAuthForm>Восстановление пароля</HeadingAuthForm>
						<Formik
							initialValues={initState}
							onSubmit={ (value: object) => {
								this.props.startedRecovery(value);
							} }
							render={() => (
								<>
								<Form>
									<Field
										component={CustomInput}
										name="email"
										placeholder="Введите email"
										validate={validate}
									/>
									<Button variant="contained" color="primary" type="submit">Отправить</Button>
								</Form>
								</>
							)}
						/>
						</> : <SuccessLabel>На email отправлено письмо с ссылкой на восстановление пароля</SuccessLabel>
						}
						</>
						: <Loading>подождите</Loading>}
					</CardContent>
				</Card>
				<MutedText><Link to="/">Назад</Link></MutedText>
			</MainWrap>
		)
	}
}

const HeadingAuthForm = styled.span`
  font-size: 0.8em;
  color: #837e7e;
  text-transform: uppercase;
`;

const MutedText = styled.div`
  color: #878585;
  margin-top: 20px;
  font-size: 0.8em;
`;

const mapStateToProps = (state: any) => {
	return{
		loading: state.loading,
		data: state.dataRecovery,
		error: state.errorRecovery
	}
}

const mapDispatchToProps = (dispatch: Dispatch) => {
	return{
		startedRecovery: bindActionCreators(startedRecovery, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordRecovery);
