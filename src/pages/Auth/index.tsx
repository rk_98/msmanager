import React, {Component} from 'react';
import styled from 'styled-components';
import {Card, CardContent, Button} from '@material-ui/core';
import {Formik, Form, Field} from 'formik';
import {Link, Redirect} from 'react-router-dom';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {CustomInput} from '../../components/CustomInput';
import {CustomPassword} from '../../components/CustomPassword';
import {SuccessLabel} from '../../components/SuccessLabel';
import {ErrorLabel} from '../../components/ErrorLabel';
import {MainWrap} from '../../components/MainWrapper';
import {Loading} from '../../components/Loading';
import { startedConfirmation, startedAuth } from './actions/actions';
import { validate } from '../../components/validate';

const initState = {
	login: '',
	password: ''
}

interface IProps {
	confirm_data?: any,
	location: any,
	error: any,
	errorAuth: any,
	auth: any,
	startedConfirmation: (token:string) => void,
	startedAuth: (values:object) => void,
	loading: boolean,
	token: string
}

class AuthPage extends Component<IProps>{

	async componentDidMount(){
		const token:any = new URLSearchParams(this.props.location.search).get("ref");
		if(token !== null){
			await this.props.startedConfirmation(token);
		}
	}

	render(){
		return(
			<MainWrap>
				<Card>
					<CardContent>
						<HeadingAuthForm>Авторизация</HeadingAuthForm>
						{!this.props.loading ?
						<Formik
							initialValues={initState}
							onSubmit={ (values: Object) => {
								this.props.startedAuth(values);
							} }
							render={() => (
								<Form>
									<Field
										component={CustomInput}
										name="email"
										placeholder="Email"
										validate={validate}
									/>
									<Field
										component={CustomPassword}
										name="password"
										placeholder="Пароль"
										validate={validate}
									/>
									<Button variant="contained" color="primary" type="submit">Подтвердить</Button>
								</Form>
							)}
						/> : <Loading>подождите</Loading> }

						{this.props.confirm_data != null ? <SuccessLabel>Пароль от вашего аккаунта: {this.props.confirm_data.data.result.password}</SuccessLabel> : null}

						{this.props.errorAuth ? <ErrorLabel>{this.props.errorAuth}</ErrorLabel> : null}

						{this.props.error ? <ErrorLabel>{this.props.error}</ErrorLabel> : null}

						{this.props.auth != undefined || this.props.token ? <Redirect to="/profile"/> : null}
					</CardContent>
				</Card>
				<MutedText><Link to="/password_recovery/">Забыли пароль?</Link></MutedText>
				<MutedText>Нет аккаунта? <Link to="/signin/">Зарегистрируйтесь</Link></MutedText>
			</MainWrap>
		)
	}
}

const HeadingAuthForm = styled.span`
	font-size: 0.8em;
	color: #837e7e;
	text-transform: uppercase;
`;

const MutedText = styled.div`
	color: #878585;
	margin-top: 20px;
	font-size: 0.8em;
`;

const mapStateToProps = (state: any, ownProps: any) => {
	return{
		confirm_data: state.confirm,
		error: state.error,
		errorAuth: state.errorAuth,
		auth: state.auth,
		loading: state.loading,
		token: state.token
	}
}

const mapDispatchToProps = (dispatch: Dispatch) => {
	return{
		startedConfirmation: bindActionCreators(startedConfirmation, dispatch),
		startedAuth: bindActionCreators(startedAuth, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);