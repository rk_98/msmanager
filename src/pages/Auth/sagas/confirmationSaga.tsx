import axios from 'axios';
import {call, put} from 'redux-saga/effects';
import {requestConfirmation, successConfirmation, failureConfirmation} from '../actions/actions';

export function* confirmationToken(action: any){
	try{
		yield put(requestConfirmation());
		console.log(action);
		const data = yield call( () => {
			return axios({
				url: 'http://msmanager.ru:45080/53c6ffac-338e-11e9-a794-002590e9c4b0_service/hs/agent-api/v2',
				method: "post",
				headers: {'content-type':'text/plain'},
				data: {
					method: "UserNewPassword",
					params: {
						key: action.payload
					}
				}
			});
		} );
		if(data.data.error){
			throw new Error('Вы уже активировали пользователя');
		}
		yield put(successConfirmation(data));
	} catch(error){
		yield put(failureConfirmation(error.message));
	}
}