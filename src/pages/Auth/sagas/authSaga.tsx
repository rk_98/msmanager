import axios from 'axios';
import {call, put} from 'redux-saga/effects';
import {requestAuth, successAuth, failureAuth} from '../actions/actions';

export function* auth(action: any){
    try{
        yield put(requestAuth());
        const data = yield call( () => {
            return axios({
                url: 'http://msmanager.ru:45080/53c6ffac-338e-11e9-a794-002590e9c4b0_service/hs/agent-api/v2',
				method: "post",
				headers: {'content-type':'text/plain'},
				data: {
					method: "GetJWT",
					params: {
                        email: action.payload.email,
                        password: action.payload.password
					}
				}
            })
        } );
        if(data.data.error){
            throw new Error('Пользователь не найден');
        }
        yield put(successAuth(data));
    } catch(error){
        yield put(failureAuth(error.message));
    }
}