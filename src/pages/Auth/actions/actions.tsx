// confirm token saga actions
export const STARTED_CONFIRMATION:string = "STARTED_CONFIRMATION";
export const REQUEST_CONFIRMATION:string = "REQUEST_CONFIRMATION";
export const SUCCESS_CONFIRMATION:string = "SUCCESS_CONFIRMATION";
export const FAILURE_CONFIRMATION:string = "FAILURE_CONFIRMATION";

export const startedConfirmation = (token: string) => {
    return{
        type: STARTED_CONFIRMATION,
        payload: token
    }
}

export const requestConfirmation = () => {
    return{
        type: REQUEST_CONFIRMATION
    }
}

export const successConfirmation = (data: object) => {
    return{
        type: SUCCESS_CONFIRMATION,
        payload: data
    }
}

export const failureConfirmation = (error: object) => {
    return{
        type: FAILURE_CONFIRMATION,
        payload: error
    }
}

// authentication saga action
export const STARTED_AUTH:string = "STARTED_AUTH";
export const REQUEST_AUTH:string = "REQUEST_AUTH";
export const SUCCESS_AUTH:string = "SUCCESS_AUTH";
export const FAILURE_AUTH:string = "FAILURE_AUTH";

export const startedAuth = (value: object) => {
    return{
        type: STARTED_AUTH,
        payload: value
    }
}

export const requestAuth = () => {
    return{
        type: REQUEST_AUTH
    }
}

export const successAuth = (data: object) => {
    return{
        type: SUCCESS_AUTH,
        payload: data
    }
}

export const failureAuth = (error: object) => {
    return{
        type: FAILURE_AUTH,
        payload: error
    }
}