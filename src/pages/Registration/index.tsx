import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import {Card, CardContent, Button} from '@material-ui/core';
import {Formik, Form, Field} from 'formik';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import {CustomInput} from '../../components/CustomInput';
import {MainWrap} from '../../components/MainWrapper';
import {validate} from '../../components/validate';
import {SuccessLabel} from '../../components/SuccessLabel';
import {ErrorLabel} from '../../components/ErrorLabel';
import {Loading} from '../../components/Loading';
import {startedRegSaga} from './actions/actions';

const initState = {
	name: '',
	second_name: '',
	email: ''
};

interface IProps {
	startedRegSaga: (values:object) => void,
	loading: boolean,
	errorReg: object,
	data?: any
}

class Registration extends Component<IProps> {

	onSubmitForm(values: object){
		this.props.startedRegSaga(values);
	}

	render(){
		return(
			<MainWrap>
				<Card>
					<CardContent>
						{!this.props.loading ?
						<>
							{this.props.data == undefined || !this.props.data.data.result ?
							<>
							<HeadingAuthForm>Регистрация</HeadingAuthForm>
							<Formik initialValues={initState}
								onSubmit={ (values: any) => {
									this.onSubmitForm(values);
								} }
								render={ () => (
									<Form>
									<Field
										component={CustomInput}
										name="name"
										placeholder="Имя"
										validate={validate}
									/>
									<Field
										component={CustomInput}
										name="second_name"
										placeholder="Фамилия"
										validate={validate}
									/>
									<Field
										component={CustomInput}
										name="email"
										placeholder="Email"
									/>
									<Button variant="contained" color="primary" type="submit">Подтвердить</Button>
								</Form>
								)}
							/>
							</> : <SuccessLabel>Вы успешно зарегистрировались. На почту отправлено письмо с ссылкой на подтверждение регистрации. Перейдите по ней, чтобы активировать аккаунт.</SuccessLabel>}
							{this.props.errorReg && <ErrorLabel>{this.props.errorReg}</ErrorLabel>}
						</>
						: <Loading>подождите</Loading>
						}
					</CardContent>
				</Card>
				{!this.props.loading && <MutedText>Уже есть аккаунт? <Link to="/">Авторизуйтесь</Link></MutedText>}
			</MainWrap>
			)
		}
	}

const HeadingAuthForm = styled.span`
	font-size: 0.8em;
	color: #837e7e;
	text-transform: uppercase;
`;

const MutedText = styled.div`
	color: #878585;
	margin-top: 20px;
	font-size: 0.8em;
`;

const mapStateToProps = (state: any) => {
	return{
		errorReg: state.errorReg,
		loading: state.loading,
		data: state.data,
		token: state.token,
		confirm_data: state.confirm
	}
}

const mapDispatchToProps = (dispatch: Dispatch) => {
	return{
		startedRegSaga: bindActionCreators(startedRegSaga, dispatch)
	}
}
	
export default connect(mapStateToProps , mapDispatchToProps)(Registration);