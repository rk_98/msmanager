import axios from 'axios';
import {call, put} from 'redux-saga/effects';
import {requestReg, successReg, failureReg} from '../actions/actions'

export function* signin(action: any){
	try{
		yield put(requestReg());
		console.log(action);
		const data = yield call( () => {
			return axios({
				url: 'http://msmanager.ru:45080/53c6ffac-338e-11e9-a794-002590e9c4b0_service/hs/agent-api/v2',
				headers: {'content-type':'text/plain'},
				method: "post",
				data: {
					method: "UserRegister",
					params: {
						fname: action.payload.name,
						lname: action.payload.second_name,
						email: action.payload.email
					}
				}
			})
		});
		if(data.data.error){
			throw new Error('Этот email уже занят, попробуйте другой');
		}
		yield put(successReg(data))
	} catch(error){
		yield put(failureReg(error.message));
	}
}