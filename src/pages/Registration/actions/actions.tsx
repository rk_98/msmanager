// sign in actions
export const STARTED_REG_SAGA:string = "STARTED_REG_SAGA";
export const REQUEST_REG:string = "REQUEST_REG";
export const SUCCESS_REG:string = "SUCCESS_REG";
export const FAILURE_REG:string = "FAILURE_REG";

export const startedRegSaga = (values: object) => {
    return{
        type: STARTED_REG_SAGA,
        payload: values
    }
}

export const requestReg = () => {
    return{
        type: REQUEST_REG
    }
}

export const successReg = (data: object) => {
    return{
        type: SUCCESS_REG,
        payload: data
    }
}

export const failureReg = (error: object) => {
    return{
        type: FAILURE_REG,
        payload: error
    }
}
