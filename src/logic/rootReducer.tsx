// import actionsTypes
import { REQUEST_REG, SUCCESS_REG, FAILURE_REG } from '../pages/Registration/actions/actions'

import {
	REQUEST_CONFIRMATION,
	FAILURE_CONFIRMATION,
	SUCCESS_CONFIRMATION,
	REQUEST_AUTH,
	SUCCESS_AUTH,
	FAILURE_AUTH,
} from '../pages/Auth/actions/actions'

import {
	REQUEST_RECOVERY,
	FAILURE_RECOVERY,
	SUCCESS_RECOVERY,
} from '../pages/PasswordRecovery/actions/actions'

import {
	LOGOUT,
	GET_MAIN_REQUEST,
	GET_MAIN_FAILURE,
	GET_MAIN_SUCCESS,
	GET_MAIN_STARTED,
	GET_SINGLE_PROFILE_REQUEST,
	GET_SINGLE_PROFILE_FAILURE,
	GET_SINGLE_PROFILE_SUCCESS
} from '../pages/Profile/actions/actions'

export const rootReducer = (state: any, action: any) => {
	switch (action.type) {
		case REQUEST_REG:
			return { ...state, loading: true }
		case SUCCESS_REG:
			return { ...state, loading: false, data: action.payload, errorReg: false }
		case FAILURE_REG:
			return { ...state, loading: false, errorReg: action.payload }

		case REQUEST_CONFIRMATION:
			return { ...state, loading: true }
		case SUCCESS_CONFIRMATION:
			return { ...state, loading: false, confirm: action.payload, error: false }
		case FAILURE_CONFIRMATION:
			return { ...state, loading: false, error: action.payload }

		case REQUEST_AUTH:
			return { ...state, loading: true }
		case SUCCESS_AUTH:
			return {
				...state,
				loading: false,
				auth: action.payload,
				errorAuth: false,
				authReq: true,
			}
		case FAILURE_AUTH:
			return { ...state, loading: false, errorAuth: action.payload }

		case REQUEST_RECOVERY:
			return { ...state, loading: true }
		case SUCCESS_RECOVERY:
			return {
				...state,
				loading: false,
				dataRecovery: action.payload,
				errorRecovery: false,
			}
		case FAILURE_RECOVERY:
			return { ...state, loading: false, errorRecovery: action.payload }

		case LOGOUT:
			return { ...state, authReq: false }
		case GET_MAIN_STARTED:
			return { ...state, loading: true, formData: null}
		case GET_MAIN_SUCCESS:
			return {
				...state,
				loading: false,
				mainData: action.payload,
				mainError: false,
			}
		case GET_MAIN_FAILURE:
			return { ...state, loading: false, mainError: action.payload }
		case GET_SINGLE_PROFILE_REQUEST:
			return{...state, loading: true}
		case GET_SINGLE_PROFILE_SUCCESS:
			return{...state, loading: false, formData: action.payload}
		case GET_SINGLE_PROFILE_FAILURE:
			return{...state, loading: false, formError: action.payload}
		default:
			return state
	}
}
