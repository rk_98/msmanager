import { takeEvery } from 'redux-saga/effects'
// import actionTypes
import { STARTED_REG_SAGA } from '../pages/Registration/actions/actions'
import { STARTED_CONFIRMATION, STARTED_AUTH } from '../pages/Auth/actions/actions'
import { STARTED_RECOVERY } from '../pages/PasswordRecovery/actions/actions'
import { GET_MAIN_STARTED, GET_SINGLE_PROFILE } from '../pages/Profile/actions/actions'
// import sagas
import { signin } from '../pages/Registration/sagas/signinSaga'
import { confirmationToken } from '../pages/Auth/sagas/confirmationSaga'
import { auth } from '../pages/Auth/sagas/authSaga'
import { recoveryPassword } from '../pages/PasswordRecovery/sagas/recoverySaga'
import { getMainData } from '../pages/Profile/sagas/getMain'
import { getSingle } from '../pages/Profile/sagas/getSingle'

export function* rootSaga() {
	yield takeEvery(STARTED_REG_SAGA, signin)
	yield takeEvery(STARTED_CONFIRMATION, confirmationToken)
	yield takeEvery(STARTED_AUTH, auth)
	yield takeEvery(STARTED_RECOVERY, recoveryPassword)
	yield takeEvery(GET_MAIN_STARTED, getMainData)
	yield takeEvery(GET_SINGLE_PROFILE, getSingle)
}
