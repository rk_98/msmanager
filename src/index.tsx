import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {BrowserRouter, Switch} from 'react-router-dom';
import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import 'normalize.css';
import './assets/css/fonts.css';

import {loadState, saveState} from './logic/localStorage';
import RootRouter from './router/rootRouter';
import {rootReducer} from './logic/rootReducer';
import {rootSaga} from './logic/rootSaga';

const prevState = loadState();

const initState = {
	token: ''
};

const sagaMiddleWare = createSagaMiddleware();

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store:any = createStore(
	rootReducer,
	prevState || initState,
	composeEnhancers(applyMiddleware(sagaMiddleWare))
);

store.subscribe(() => {
	if(store.getState().authReq){
		saveState({
			token: store.getState().auth.data.result
		})
	} else {
		if(store.getState().token){
			//store.setState({authReq: true})
		} else{
			saveState({
				token: ''
			})
		}
	}
});

sagaMiddleWare.run(rootSaga);

ReactDOM.render(
		<Provider store={store}>
			<BrowserRouter>
				<Switch>
					<RootRouter />
				</Switch>
			</BrowserRouter>
		</Provider>
	, document.getElementById('root'));